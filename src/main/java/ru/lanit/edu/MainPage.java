package ru.lanit.edu;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    @FindBy(id = "text")
    private WebElement input;
    @FindBy(xpath = "//button")
    private WebElement button;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void search() {
        input.sendKeys("костюм аниме тян");
        button.click();
    }

}
